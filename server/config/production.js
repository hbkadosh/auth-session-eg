'use strict';

var domain_name = "http://ec2-54-169-78-232.ap-southeast-1.compute.amazonaws.com";

module.exports = {
    mysql: process.env.MYSQL_DATABASE_URL,
    domain_name: domain_name,
    aws: {
        id: "AKIA",
        key: "+AwRTzS3QntviHlrkJgs",
        url: "https://nus-stackup.s3.amazonaws.com",
        bucket: "nus-stackup",
        region: "ap-southeast-1"
    },
    mailgun_key: "key-241cb37d3087b",
    mailgun_domain: "sandboxe287.mailgun.org",
    register_email: {
        from: "Weddinggram User <noreply@weddinggram.sg>",
        subject: "Welcome to StackUp Weddinggram",
        email_text: "Hello! Thank you for registering with Stackup WeddingGram. Have fun !"
    },
    reset_password_email: {
        from: "noreply@weddinggram.sg",
        subject: "WeddingGram Password Reset",
        email_text: "Hello! You recently requested a link to reset your password. Please set a new password on the following link !",
        email_content: "Hello! <br>You recently requested a link to reset your password. <br>Please set a new password on the following link <br>"
    },
    port: process.env.PORT || 3000,
    seed: process.env.SEED || false,
    linkedin_key: "81vz",
    Linkedin_secret: "Lx5RmZ",
    Linkedin_callback_url: domain_name + "/oauth/linkedin/callback",
    GooglePlus_key: "55uiv.apps.googleusercontent.com",
    GooglePlus_secret: "JbU_7_j8p7f-t",
    GooglePlus_callback_url: domain_name + "/oauth/google/callback",
    Facebook_key: "1774596",
    Facebook_secret: "bbd01e",
    Facebook_callback_url: domain_name + "/oauth/facebook/callback",
    Twitter_key: "IsKz8sm",
    Twitter_secret: "fiyT7xCRhZXd",
    Twitter_callback_url: domain_name + "/oauth/twitter/callback",
    Wechat_AppId: "1",
    Wechat_Name: "1",
    Wechat_AppSecret: "1",
    Wechat_Callback_Url: domain_name + "/oauth/wechat/callback"
};
